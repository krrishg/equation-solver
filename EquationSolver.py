import re

class EquationSolver:

    def solve(self, equation):
        expression = self.__extract_expression(equation)
        return self.__evaluate(expression)


    def __extract_expression(self, equation):
        return equation.split('=')[0]


    def __evaluate(self, expression):
        operands = self.__extract_operands(expression)
        return (int(expression) if self.__has_no_operator(operands) 
                    else self.__get_result(operands, expression))


    def __extract_operands(self, expression):
        operators = '[+ \- \* \/]'
        return re.split(operators, expression)


    def __has_no_operator(self, operands):
        return len(operands) == 1


    def __get_result(self, operands, expression):
        operator = self.__find_operator(expression)
        return (expression if operator is None 
                    else self.__get_result_for_non_null_operators(operands, 
                        expression, operator))


    def __get_result_for_non_null_operators(self, operands, expression, 
            operator):

        result = self.__perform_operation(operands[0], operands[1], operator)

        for index,operand in enumerate(operands):
            if index > 1:
                expression = (self.__get_expression_after_first_operator(
                                expression, operator))
                operator = self.__find_operator(expression)
                result = self.__perform_operation(result, operand, operator)

        return result


    def __get_expression_after_first_operator(self, expression, operator):
        return expression.split(operator, 1)[1]


    def __find_operator(self, expression):
        for s in expression:
            if self.__is_operator(s):
                return s


    def __is_operator(self, operator):
        return (operator == '+' or 
                operator == '-' or 
                operator == '*' or 
                operator == '/')


    def __perform_operation(self, first_operand, second_operand, operator):
        if operator == '+':
            return int(first_operand) + int(second_operand)
        elif operator == '-':
            return int(first_operand) - int(second_operand)
        elif operator == '*':
            return int(first_operand) * int(second_operand)
        elif operator == '/':
            return int(first_operand) / int(second_operand)
