import unittest
import EquationSolver

class EquationTest(unittest.TestCase):
    equationSolver = None

    def setUp(self):
        self.equationSolver = EquationSolver.EquationSolver()


    def test_no_operation(self):
        self.assertEqual(1, self.equationSolver.solve('1=x'));


    def test_add(self):
        self.assertEqual(3, self.equationSolver.solve('1+2=x'))
        self.assertEqual(4, self.equationSolver.solve('2+2=x'))
        self.assertEqual(5, self.equationSolver.solve('1+2+2=x'))
        self.assertEqual(15, self.equationSolver.solve('1+2+5+3+4=x'))


    def test_subtract(self):
        self.assertEqual(0, self.equationSolver.solve('2-2=x'))
        self.assertEqual(-1, self.equationSolver.solve('2-3=x'))
        self.assertEqual(0, self.equationSolver.solve('5-2-3=x'))
        self.assertEqual(3, self.equationSolver.solve('10-2-2-3=x'))


    def test_multiply(self):
        self.assertEqual(6, self.equationSolver.solve('2*3=x'))
        self.assertEqual(30, self.equationSolver.solve('2*3*5=x'))
        self.assertEqual(0, self.equationSolver.solve('2*3*1*0=x'))


    def test_divide(self):
        self.assertEqual(2, self.equationSolver.solve('4/2=x'))
        self.assertEqual(2, self.equationSolver.solve('4/2/1=x'))
        self.assertEqual(5, self.equationSolver.solve('50/5/2=x'))


    def test_integration(self):
        self.assertEqual(2, self.equationSolver.solve('2+1-1=x'))
        self.assertEqual(2, self.equationSolver.solve('3*1-1=x'))
        self.assertEqual(6, self.equationSolver.solve('3*1-1+2+2=x'))
        self.assertEqual(6, self.equationSolver.solve('2+1-1*3=x'))
        self.assertEqual(10, self.equationSolver.solve('2+1-1/2*10=x'))


    if __name__ == 'main':
        unittest.main()
